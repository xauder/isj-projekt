# -*- coding: utf-8 -*-
from flask import render_template, request, make_response
from threading import Thread
from app import app, webdata
from data_download.settings import EXPORT_FILENAME

#-----
# All page views for web application. Importing teemplates from
# app/templates/ for each view
#-----
@app.route('/')
@app.route('/index.html')
def index():
    regions, regionsData = webdata.regionStats()
    return render_template('index.html', regions=regions, regionsData=regionsData)

@app.route('/about')
def about():
    return render_template('about.html', activePage='about')

@app.route('/places/<letter>')
@app.route('/places')
def places(letter=None):
    if not letter or len(letter) != 1:
        letter = 'A'
    content = webdata.cityList(letter)
    return render_template('places.html', content=content)

@app.route('/detail/<name>')
def detail(name=None):
    content = [ a.name for a in webdata.cityList()]
    if not name in content:
        return render_template('404.html'), 404
    city, additional = webdata.cityDetail(name)
    kindergartens = webdata.kindergartenList(city.id)
    return render_template('detail.html', city=city, additional=additional, kg=kindergartens)

@app.route('/search', methods = ['POST'])
def search():
    name = request.form['str']
    if not name:
        return render_template('404.html'), 404
    content = webdata.cityList(string=name)
    return render_template('search.html', content=content, string=name)

#-----
# Only export to csv_en works properly now ( csv_cz doesnt respect
# encoding and json is not present at all)
#-----
@app.route('/export/<file>')
def export(file=None):
    if file == 'csv_cz':
        file = 'csv'
        export = webdata.exportWebCsv(sep='.',encoding='windows-1250')
    elif file == 'csv_en':
        file = 'csv'
        export = webdata.exportWebCsv()
    else:
        return render_template('404.html'), 404
    response = make_response(export)
    response.headers["Content-Disposition"] = "attachment; filename=" + EXPORT_FILENAME + "." + file
    return response

@app.route('/raw')
def raw():
    tabContent = webdata.kindergartenList()
    tabCity = webdata.cityList()
    return render_template('raw.html', tabContent=tabContent)

@app.errorhandler(404)
def pageNotFound(e):
    return render_template('404.html'), 404

