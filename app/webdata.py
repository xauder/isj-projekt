# -*- coding: utf-8 -*-
import models
from string import ascii_uppercase
from data_download.settings import EXPORT_CSV_ORDER

#-----
# Returns statistics for each region
#-----
def regionStats():
    regionManager = models.RegionManager()
    regionList = list()
    regionStats = list()
    for i in range(1,15):
        regionList.append(regionManager.getItemById(i).name)
        regionStats.append(regionManager.getItemById(i).getSummary())
    return regionList, regionStats

#-----
# Returns list of cities. Optionally returns listo fo cities which
# names begin on specific letter or containig some string
#-----
def cityList(letter=None, string=None):
    cityManager = models.CityManager()
    cityNameList = list()
    if letter:
        cityNameList = cityManager.getByFirstLetter(letter)
    elif string:
        cityNameList = cityManager.getByStringContained(string)
    else:
        for letter in ascii_uppercase:
            cityNameList = cityNameList + cityManager.getByFirstLetter(letter)
    return cityNameList

#-----
# Returns all kindergartens in town
#-----
def kindergartenList(id=None):
    kgManager = models.KindergartenManager()
    if id:
        return kgManager.getByCity(id)
    return kgManager.getAll()

#-----
# Returns details for desired city ans come aditional data not stored in
# City object
#-----
def cityDetail(name):
    cityManager = models.CityManager()
    city = cityManager.getItemByName(name)
    if type(city.region) is int:
        regionManager = models.RegionManager()
        region = regionManager.getItemById(city.region)
        region = region.name
    else:
        region = city.region
    additional = list()
    additional.append(city.getPreschoolCapacitySum())
    additional.append(city.getPreschoolsCount())
    additional.append(region)
    return city, additional

#-----
# Creates export file for webapp
#-----
def exportWebCsv(sep=',', encoding=None):
    out = ''
    content = kindergartenList()
    cityManager = models.CityManager()
    out += ','.join(EXPORT_CSV_ORDER) + '\n'
    for item in content:
        city = cityManager.getItemById(item.city)
        out += '"' + city.name + '"' + sep + ' "' + \
                     item.name + '"' + sep + ' "' + \
                     item.address + '"' + sep + ' "' + \
                     str(item.capacity) + '"\n'
    if encoding:
        out.encode(encoding)
    return out
