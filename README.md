Projekt do predmetu ISJ: Materske skoly
=======================================

Autori:
-------

Tomas Coufal <xcoufa09(at)stud.fit.vutbr.cz>
Milos Svana <xsvana01(at)stud.fit.vutbr.cz>

Zakladne informacie:
--------------------

Projekt sa zaobera ziskanim a spracovanim dat o materskych skolach v Ceskej
republike. Data cerpame z webovych stranok http://rejskol.msmt.cz/ a 
http://www.rozpocetobce.cz/. Projekt je rozdeleny na dve casti:

1. Balicek data_download obsahuje zakladnu funkcionalitu na ziskavanie dat
   zo spominanych zdrojov. Skript main.py v tomto baliku je mozne spustit
   z terminalu ako program. V takom pripade vygeneruje zoznam materskych skol
   v Ceskej republike, ktory ulozi do suborov vo formate CSV a JSON

2. Webova aplikacia zobrazujuca data o materskych skolach v kombinacii s udajmi
   ziskanych z projektu rozpocetobce.cz. Pre kazdy kraj zobrazuje suhrnne 
   statistiky a umoznuje ziskavat podrobnosti o materskych skolach v jednotlivych
   obciach. Ci stiahnut CSV export

Pouzite kniznice:
-----------------

Dolezite: Aplikacia bola vytvorena pre Python 3 (testovane na verzii 3.4.0)

V zozname neuvadzam kniznice, ktore su sucastou instalacie jazyka Python
ako napriklad datetime alebo csv:

BeautifulSoup
Flask
Requests
SQL Alchemy

Ako s aplikaciou pracovat:
--------------------------

Na ziskanie stiahnutie zoznamu materskych skol a vygenerovanie
CSV a JSON suboru staci zavolat:

python3 data_download/main.py

Databazu pre webovu aplikaciu uz dodavame spolu s aplikaciou. Je vsak
mozne ju vygenerovat aj manualne (pred generovanim je nutne staru 
databazu vymazat):

python3 models/create_tables.py
python3 models/fill_tables.py

Ulozenie niektorych materskych skol moze hlasit failed. Jedna sa o pripad, kedy obec,
v ktorej sa skolka nachadza nie je v zozname, ktory sa ziskava z rozpocetobce.cz

Spustenie webovej aplikacie sa uskutocni pomocou:

python3 webapp.py

Aplikacia je nasledne pristupna na http://127.0.0.1:5000/
