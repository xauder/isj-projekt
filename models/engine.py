#!/usr/bin/env python3

from sqlalchemy import create_engine
from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declared_attr, declarative_base

import os

modelsPackagePath = os.path.dirname(os.path.abspath(__file__))
dbPath = os.path.join(modelsPackagePath, 'db.sqlite')
dbEngine = create_engine('sqlite:///' + dbPath)

class Model(object):
    id = Column(Integer, primary_key=True)

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

Model = declarative_base(cls=Model)
