#!/usr/bin/env python3

#-----
# Contains classes for managing rows of database tables. These may implement
# methods for finding, saving or deleting records and similiar operations
#-----

from sqlalchemy.orm import sessionmaker
from models.tables import Kindergarten, City, Region
from models.engine import dbEngine


#-----
# Binds all operations to a specific engine which represents concrete database
#-----

DbSession = sessionmaker(dbEngine)


#-----
# Basic class for managing a database table. For each table should exist a 
# child class and DbManager should be never used directly - it's an abstract
# class.
# 
# The child classes should have a managedClass attribute for storing a class 
# (SQL alchemy mapping) representing the table whose rows will be handled by 
# the manager. 
#-----

class DbManager(object):
    dbSession = DbSession()

    #-----
    # Executes all database write queries. No real changes in the database are 
    # made until this function is called or some of them necessesary.
    #
    # There is no need to call this method when making read requests.
    #-----

    @classmethod
    def commit(cls):
        cls.dbSession.commit()

    #-----
    # Finds a row with the given id. Returns an instance of the managed SQL
    # alchemy mapper class.
    #
    # In the children classes the managedClass attribute has to be set 
    # otherwise this method will not work
    #-----

    @classmethod
    def getItemById(cls, id):
        return cls.dbSession.query(cls.managedClass).filter_by(id=id).first()

    #-----
    # Finds a row where the name column is set to the given value. Many tables
    # in the application such as city or region have a name column so it's
    # helpful to have an universal method for this
    #
    # In the children classes the managedClass attribute has to be set 
    # otherwise this method will not work
    #-----

    @classmethod
    def getItemByName(cls, name):
        return cls.dbSession.query(cls.managedClass).filter_by(name=name).first()

    #-----
    # Saves an instance of some sql mapper class which actually represents 
    # table row to the database
    #-----

    @classmethod
    def saveItem(cls, item):
        return cls.dbSession.add(item)

    @classmethod
    def getAll(cls):
        return cls.dbSession.query(cls.managedClass).all()

#-----
# Manages the table of regions
#-----

class RegionManager(DbManager):
    managedClass = Region

    @classmethod
    def calculateEachRegionSummary(cls):
        allRegions = cls.getAll()
        for region in allRegions:
            region.calculateSummary()
            cls.saveItem(region)
        cls.commit()


#-----
# Manages the table of cities
#-----

class CityManager(DbManager):
    managedClass = City

    #-----
    # Gets all cities located in a region specified by it's ID. 
    # Returns an instance of tables.City SQL alchemy mapper class
    #-----

    @classmethod
    def getByRegion(cls, regionId):
        cities = cls.dbSession.query(City).filter_by(region=regionId)
        return cities.all()

    #-----
    # Gets a list of cities whose name begins with the certain letter
    # each element of the list is a table.City instance
    #-----

    @classmethod
    def getByFirstLetter(cls, letter):
        citiesQuery = cls.dbSession.query(City)
        citiesFiltered = citiesQuery.filter(City.name.like('%s%%'%letter))
        cities = citiesFiltered.order_by(City.name)
        return cities.all()

    #-----
    # Gets a list of cities whose name contains the given string
    # each element of the list is a table.City instance
    #-----

    @classmethod
    def getByStringContained(cls, string):
        cities = cls.dbSession.query(City).filter(City.name.like('%%%s%%'%string))
        return cities.all()

#-----
# Manages the table of kindergartens
#-----

class KindergartenManager(DbManager):
    managedClass = Kindergarten

    #-----
    # Gets all kindergartens located in a city specified by it's ID.
    # Returns an instance of tables.Kindergarten SQL alchemy mapper class
    #-----

    @classmethod
    def getByCity(cls, cityId):
        kindergartens = cls.dbSession.query(Kindergarten).filter_by(city=cityId)
        return kindergartens.all()
