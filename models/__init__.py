#!/usr/bin/env python3

from models.tables import Region, City, Kindergarten 
from models.managers import KindergartenManager, CityManager, RegionManager

"""
---------------------
Ako pouzivat databazu
---------------------

1. Vytvorenie a naplnenie databazy:
-----------------------------------
Databaza a jej tabulky sa vytvoria pomocou skriptu create_tables.py,
staci ho spustit bez parametrov:

    ./models/create_tables.py

Databaza sa ulozi ako db.sqlite v adresari models.
Na naplnenie databazy datami sa pouziva skript fill_tables.py, rovnako
ako create_tables ho staci spustit bez parametrov:

    ./models/fill_tables.py

2. Ziskavanie dat z databazy:
-----------------------------

Na pracu s datami v databaze sluzia predovsetkym triedy z modulu 
./models/managers. Na ich pouzitie vsak staci importovat balik models,
su pristupne z jeho menneho priestoru:

    import models
    models.CityManager.getItemById(2)

Pre kazdu tabulku existuje jeden osobitny manager:

    KindergartenManager
    CityManager
    RegionManager

Kazdy s tychto manazerov poskytuje nasledujuce metody:

    getItemById(id):
        Vrati objekt z databazy, ktory ma pridelene dane id. Najdeny zaznam 
        vrati ako instanciu triedy tables.City, tables.Region alebo 
        tables.Kindergarten

        Napriklad: models.CityManager.getItemById(1)
        vrati mesto, ktore je ulozene pod id 1, co je Praha

    getItemByName(name):
        Funguje podobne ako getItemById, akurat ziska z databazy mesto s danym 
        menom.

        Napriklad: models.RegionManager.getItemByName('Jihomoravský kraj')
        vrati objekt typu tables.Region reprezentujuci Jihomoravsky kraj

CityManager ma naviac metody:

    getByRegion(regionId):
        Ziska zoznam vsetkych miest, ktore sa nachadzaju v kraji s danym ID
        kazde mesto je objekt typu tables.City

        Napriklad:
        jihomoravskyRegion = models.RegionManager.getItemByName('Jihomoravský kraj')
        cities = CityManager.getByRegion(jihomoravskyRegion.id)

    getByFirstLetter(letter):
        Ziska zoznam vsetkych miest, ktorych meno zacina danym pismenom

    getByStringContained(string):
        Ziska zoznam vsetkych miest, v ktorych nazve sa nachadza dany retazec
        Uzitocne pri vyhladavani

KindergartenManager ma naviac metody:
    
    getByCity(cityId):
        Funguje podobne ako CityManager.getByRegion, akurat ziska skolky,
        ktore sa nachadzaju v meste s danym id

        Napriklad:
        brno = CityManager.getItemByName('Brno')
        kindergartens = KindergartenManager.getByCity(brno.id)

Vsetky vyssie uvedene metody vracaju objekty typu tables.City, tables.Region
alebo tables.Kindergarten. Podme sa na ne pozriet:

Trieda Kindergarten ma tieto atributy:
    id - id skolky
    name - meno skolky
    address - adresa v ramci mesta
    city - id mesta, kde sa skolka nachadza

Trieda City ma tieto atributy a metody:
    id - id mesta
    population - pocet obyvatelov
    preschoolExpenses - vydaje na predskolske zariadenia
    region - id kraja, kde sa mesto nachadza

    getPreschoolsCount() - vrati pocet skolek v danom meste

        Napriklad:
        brno = CityManager.getItemByName('Brno')
        preschoolsCount = brno.getPreschoolsCount()

    getPreschoolCapacitySum() - vrati celkovu kapacitu skolek

Trieda Region ma tieto atributy a metody:
    id - id kraja
    name - meno kraja

    getSummary() - vrati suhrnne info o kraji v podobe slovnika,
    ktory obsahuje nasledujuce kluce:
        citiesCount - pocet miest v kraji
        population - pocet obyvatelov
        preschoolCapacitySum - celkova kapacita skolek
        preschoolExpensesPerPerson - vydaje na skolky na jednu osobu
        preschoolExpensesSum - celkove vydaje obci na skolky
        preschoolsCount - pocet skolek v kraji

        Napriklad:
        hmPraha = RegionManager.getItemById(1)
        summary = hmPraha.getSummary()
        print(summary['population'])
"""
