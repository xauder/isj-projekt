#!/usr/bin/env python3

#-----
# This file contains definitions of database objects: tables and operations
# which work with one row of the table
#-----

from sqlalchemy import BigInteger, Column, ForeignKey, Integer, String, Float
from models.engine import dbEngine, Model


#-----
# Czech republic is divided into 14 regions, the Region class represents them 
# in the database.
#
# Also provides summary data needed for web application output:
#  - population
#  - count of cities
#  - count of preschool devices
#  - total capacity of prescool devices
#  - total expenses on preschool devices
#  - expenses on preschool devices per person
#
# To access this information simply call the getSummary() method.
#-----

class Region(Model):
    name = Column(String)

    # Region summary
    citiesCount = Column(Integer)
    population = Column(Integer)
    preschoolCapacitySum = Column(Integer)
    preschoolExpensesSum = Column(BigInteger)
    preschoolExpensesPerPerson = Column(Float)
    preschoolsCount = Column(Integer)

    def calculateSummary(self):
        """ 
        The function itself does not save anything to the database
        To do so, use RegionManager class like in this example:

            region.calculateSummary()
            RegionManager.saveItem(region)
            RetionManager.commit()

        """
        from models import CityManager
        self.__setSummaryToNull()
        citiesInRegion = CityManager.getByRegion(self.id)
        for city in citiesInRegion:
            self.__addCityToSummary(city)
        self.__calculatePreschoolExpensesPerPerson()

    #-----
    # Returns a dictionary containing summary of the region. You can find more
    # about what summary is in the class comment.
    #-----

    def getSummary(self):
        return self.__buildStatisticsDict()

    #-----
    # Sets all summary data to null before the getSummary() method starts 
    # generating them from information about cities in the region.
    # 
    # For example if we want to count the population of the region we simply
    # start at zero and then we just add population of each city to the sum
    #-----

    def __setSummaryToNull(self):
        self.citiesCount = 0
        self.population = 0
        self.preschoolExpensesPerPerson = 0
        self.preschoolExpensesSum = 0
        self.preschoolCapacitySum = 0
        self.preschoolsCount = 0

    #-----
    # Adds information from one city to the region summary. Does not affect
    # preschools expenses per person which are calculatet when all cities in
    # the region are in the summary
    #-----

    def __addCityToSummary(self, city):
        self.citiesCount += 1
        self.population += city.population
        self.preschoolCapacitySum += city.getPreschoolCapacitySum()
        self.preschoolExpensesSum += city.preschoolExpenses
        self.preschoolsCount += city.getPreschoolsCount()

    #-----
    # Calculates the amount of money spent on preschool devices per one person
    # after all cities in the region are included in the region summary
    #-----

    def __calculatePreschoolExpensesPerPerson(self):
        self.preschoolExpensesPerPerson = \
            self.preschoolExpensesSum / self.preschoolCapacitySum

    #-----
    # Creates a dictionary containing region summary which will be returned
    # by getStatistics() methon when data generation is finished
    #-----

    def __buildStatisticsDict(self):
        return dict(
            citiesCount=self.citiesCount,
            population=self.population,
            preschoolCapacitySum=self.preschoolCapacitySum,
            preschoolExpensesPerPerson=self.preschoolExpensesPerPerson,
            preschoolExpensesSum=self.preschoolExpensesSum,
            preschoolsCount=self.preschoolsCount)


#-----
# Represents a city (or village) in the database. For each city we need to 
# know it's name, population, the region which the city belongs in and the
# the amount of money spent on preschool devices.
# 
# Some other information like preschools count or total capacity of 
# preschools devices are generated dynamically using kindergarten table
#-----

class City(Model):
    name = Column(String(128))
    population = Column(Integer, nullable=False)
    preschoolExpenses = Column(BigInteger, nullable=False)
    region = Column(Integer, ForeignKey('region.id'), nullable=False)

    #-----
    # Returns the count of preschool devices in the city. Does not
    # use any kind of cache, so every time the function is called, the
    # counting starts from beginning
    #-----

    def getPreschoolsCount(self):
        from models import KindergartenManager
        preschools = KindergartenManager.getByCity(self.id)
        return len(preschools)

    #-----
    # Returns the sum of all preschool devices capacities in the city
    # Does not use any kind of cache, so every time the function is called, 
    # the counting starts from beginning
    #-----

    def getPreschoolCapacitySum(self):
        from models import KindergartenManager
        preschools = KindergartenManager.getByCity(self.id)
        preschoolsCapacity = 0
        for preschool in preschools:
            preschoolsCapacity += preschool.capacity
        return preschoolsCapacity

    #-----
    # Creates new city object from a dictionary returned by 
    # BudgetPage.parseCsv() method. Does not save it.
    #-----

    @classmethod
    def createFromDict(cls, cityDict):
        from models import RegionManager
        region = RegionManager.getItemByName(cityDict['region'])
        city = City(
            name=cityDict['name'], 
            population=cityDict['population'],
            preschoolExpenses=cityDict['preschoolExpenses'], 
            region=region.id)
        return city

#-----
# Represents a kindergarten (preschool device) in the database. Each
# kindergarten in located in a certain city on some address, has a name
# and a capacity of children
#-----

class Kindergarten(Model):
    address = Column(String(128))
    capacity = Column(Integer)
    city = Column(Integer, ForeignKey('city.id'))
    name = Column(String(128))

    #-----
    # Creates new kindergarten object from a dictionary returned by 
    # fetchKindergarterns() function. Does not save it.
    #-----

    @classmethod
    def createFromDict(cls, kindergartenDict):
        from models import CityManager
        city = CityManager.getItemByName(kindergartenDict['city'])
        kindergarten = Kindergarten(
            address=kindergartenDict['address'],
            name=kindergartenDict['name'],
            capacity=kindergartenDict['capacity'],
            city=city.id)
        return kindergarten
