#!/usr/bin/env python3

import os

thisFilePath = os.path.abspath(__file__)
thisFileDir = os.path.dirname(thisFilePath)
parentDir = os.path.dirname(thisFileDir)

os.sys.path.insert(0, parentDir)


""" 
Downloads all data and fills the database

Can be executed directly from terminal. No parameters are required. The 
database must already exist and contain empty tables. To create an emtpy
database use the create_tables.py script

"""

from models import CityManager, KindergartenManager, RegionManager
from models import City, Kindergarten, Region

from data_download.pages_budget import BudgetPage
from data_download.main import fetchKindergartens


regionNames = [
    'Hlavní město Praha',
    'Jihomoravský kraj',
    'Moravskoslezský kraj',
    'Liberecký kraj',
    'Plzeňský kraj',
    'Kraj Vysočina',
    'Olomoucký kraj',
    'Jihočeský kraj',
    'Královéhradecký kraj',
    'Pardubický kraj',
    'Středočeský kraj',
    'Zlínský kraj',
    'Ústecký kraj',
    'Karlovarský kraj' ]


def fillRegionsTable(regionNames, printOutput=True):
    """
    Creates a record for each region in Czech rebublic.

    The regionNames argument should be a list of strings. By printOutput
    argument you can say if the function should print what is it doing right 
    now.

    """
    if printOutput: print('Saving regions ...')

    for regionName in regionNames:
        if printOutput: print('Adding %s ...' %regionName)
        region = Region(name=regionName)
        RegionManager.saveItem(region)

    RegionManager.commit()


def fetchCities(printOutput=True):
    """
    Downloads a list of cities in Czech republic from a web source.

    Each city is a dictionary containing these keys: name, region, capacity,
    address and city. Only cities which spend money on preschool devices are
    included.

    """
    if printOutput: print('Downloading list of cities ...')

    citiesPage = BudgetPage()
    citiesPage.loadPage()
    cities = citiesPage.parseCsv()

    return cities


def fillCitiesTable(cities, printOutput=True):
    """
    Puts the data downloaded by fetchCities() function to the DB.

    By printOutput argument you can say if the function should print what
    is it doing right now.

    """
    if printOutput: print('Saving cities ...')

    for cityDict in cities:
        if printOutput: print('Adding %s ...' %cityDict['name'])
        city = City.createFromDict(cityDict)
        CityManager.saveItem(city)

    CityManager.commit()


def fillKindergartensTable(kindergartens, printOutput=True):
    """
    Puts the data downloaded by fetchKindergartes() function to the DB.

    The adding is in a try-except block, because we only store cities,
    which spend some money on preschool devices, but kindergarten can
    also be in city which is not in database.

    """
    if printOutput: print('Creating kindergartens ...')

    for kindergartenDict in kindergartens:
        if printOutput: print('Adding %s ...' %kindergartenDict['name'])
        try:
            kindergarten = Kindergarten.createFromDict(kindergartenDict)
            KindergartenManager.saveItem(kindergarten)
        except:
            if printOutput: print('Failed!')

    KindergartenManager.commit()


if __name__ == '__main__':
    fillRegionsTable(regionNames)
    cities = fetchCities()
    fillCitiesTable(cities)
    print('Downloading kindergartens. This will take a few minutes ...')
    kindergartens = fetchKindergartens()
    fillKindergartensTable(kindergartens)
    RegionManager.calculateEachRegionSummary()
