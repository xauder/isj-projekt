#!/usr/bin/env python3
import os
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parentdir)

from models import City, Kindergarten, Region
from models.engine import dbEngine

if __name__ == '__main__':
    Region.metadata.tables['region'].create(bind=dbEngine)
    City.metadata.tables['city'].create(bind=dbEngine)
    Kindergarten.metadata.tables['kindergarten'].create(bind=dbEngine)
