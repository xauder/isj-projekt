# -*- coding: utf-8 -*-
#!/usr/bin/env python3
#-----

from bs4 import BeautifulSoup
from threading import Thread, Lock
from page import Page
from settings import *

import re
import requests

#-----
# Loads and parses information from the search form page. We need at least 
# the list of regions and the list of validation data to make successful
# POST search request
#-----

class FormPage(Page):

    #-----
    # Default values sent on search POST request
    #-----

    postData = {
        'btnVybrat': 'Vybrat',
        'ctl38': '',
        'ctl39': '',
        'cmdZrizovatel_dD': '',
        'txtPocetZaznamu': '400',
        '__EVENTVALIDATION': '',
        '__VIEWSTATE': '' }

    #-----
    # Extracts validation data which are required to make a successful 
    # search POST request 
    #-----

    def getPostData(self):
        if not self.loaded:
            return False
        viewState = self.getElementByName('__VIEWSTATE')['value']
        self.postData['__VIEWSTATE'] = viewState
        eventValidation = self.getElementByName('__EVENTVALIDATION')['value']
        self.postData['__EVENTVALIDATION'] = eventValidation
        return self.postData

    #-----
    # Returns a list of regions which is usable in other parts of application
    # Each region is represented as a dictionary containing it's id and name 
    #-----

    def getRegions(self):
        if not self.loaded:
            return False
        optionTags = self.getElementByName('ctl39').find_all('option')
        regions = list()
        for optionTag in optionTags:
            if(self.isOkres(optionTag)):
                regions.append(self.optionToRegion(optionTag))
        return regions

    #----- 
    # Checks if the given option tag contains a region whose type is something 
    # we call "okres". Description of these starts with the string "CZ0ddd"
    #-----

    def isOkres(self, optionTag):
        return optionTag.string and \
            re.match('^CZ0[0-9A-Z]{3}', optionTag.string)

    #-----
    # Transfers the HTML option tag for region selection to a dictionary
    # containing region id and it's name
    #-----

    def optionToRegion(self, optionTag):
        regionName = FormPage.trimRegionCode(optionTag.string)
        return dict(id=optionTag['value'], name=regionName)

    #-----
    # First 8 characters of the region name extracted from search 
    # form page are a code such as CZ0111, which has to be removed
    #-----

    @staticmethod
    def trimRegionCode(regionName):
        return regionName[8:].strip()


#-----
# Searches the given region and finds all kindergartens in it. Each
# instance of this class runs as a standalone process
#-----

class RegionList(Thread, Page):
    lock = Lock()
    kindergartens = list()

    #-----
    # Sets the basic attributes for one specific region. We need to know
    # it's id and name and also some basic POST data which are same for 
    # each region
    #-----

    def __init__(self, region, postData, **kwargs):
        Page.__init__(self, '')
        Thread.__init__(self, **kwargs)
        self.session = requests.session()
        self.postData = postData
        self.regionId = region['id']
        self.regionName = region['name']

    #-----
    # The main function of the process. Loads the page containing  a list
    # of kindergartens in the region and parses the recieved information
    #-----

    def run(self):
        self.loadPage()
        detailUrls = self.getDetailUrls()
        for detailUrl in detailUrls:
            self.url = SCHOOLS_DIRECTORY_WEBPAGE + detailUrl['href'][2:]
            detailPage = Page.loadPage(self, refresh=True)
            kindergartenDetails = self.parseDetails(detailPage)

            # Protected by lock - lightweight semaphore
            self.lock.acquire()
            if not kindergartenDetails in self.kindergartens:
                self.kindergartens.append(kindergartenDetails)
            self.lock.release()

    #-----
    # Returns a list of pages containing details about each of kindergraten
    # found
    #-----

    def getDetailUrls(self):
        aElems = self.soup.find_all('a', href=re.compile(r'PravOsoba'))
        return aElems

    #-----
    # Sends a HTTP request for searching the kindergartens in the region
    # returns a BeautifulSoup HTML object
    #-----

    def loadPage(self):
        if not self.url:
            self.url = SCHOOLS_DIRECTORY_WEBPAGE + SCHOOLS_FORM_URI
        params = self.getPostParams()
        return Page.loadPage(self, method='POST', params=params)

    #-----
    # Prepares POST paremeters for the search request. These can be set
    # ctl38 - school type - A for kindergarten
    # ctl39 - key of the region to search
    #-----

    def getPostParams(self):
        self.postData['ctl38'] = SCHOOLS_TYPE_KINDERGARTEN
        self.postData['ctl39'] = self.regionId
        return self.postData

    #-----
    # Creates a dictionary containing information about concrete 
    # kindergarten. Accepts the BeautifulSoup object of the detail page
    #-----

    def parseDetails(self, detailPage):
        details = dict()
        details['name'] = detailPage.find(id='lblJmenoPravOsoby').text
        for row in detailPage.find(id='DataListSeznam').find_all('tr'):
            i = 0
            for col in row.find_all('td'):
                if i == 1 and col.text != 'A00': break
                if i == 3: details['city'] = RegionList.trimCityParts(col.text)
                if i == 4: details['address'] = col.text
                if i == 6: details['capacity'] = int(col.text)
                i+=1
        return details


    @staticmethod
    def trimCityParts(cityName):
        numbersRegexp = re.compile('[0-9]+')
        cityPartsRegexp = re.compile('\s-\s.*$')
        noNumbersCityName = re.sub(numbersRegexp, '', cityName)
        noPartsCityName = re.sub(cityPartsRegexp, '', noNumbersCityName)
        return noPartsCityName.strip(' \n')
