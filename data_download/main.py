#!/usr/bin/env python3

from export import exportCsv, exportJson
from pages import FormPage, RegionList
from settings import SCHOOLS_DIRECTORY_WEBPAGE
from settings import SCHOOLS_FORM_URI

def fetchKindergartens():
    formPage = FormPage(SCHOOLS_DIRECTORY_WEBPAGE + SCHOOLS_FORM_URI)
    formPage.loadPage()
    regions = formPage.getRegions()
    postData = formPage.getPostData()

    regionThreads = list()

    for region in regions:
        regionList = RegionList(region, postData)
        regionThreads.append(regionList)
        regionList.start()

    for regionThread in regionThreads:
        regionThread.join()

    return RegionList.kindergartens

if __name__ == '__main__':
    print('Downloading list of kindergartens. This may take a few minutes ...')
    kindergartens = fetchKindergartens()
    print('Exporting files ...')
    exportCsv(kindergartens)
    exportJson(kindergartens)

