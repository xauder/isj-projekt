# -*- coding: utf-8 -*-
#!/usr/bin/env python3
#-----

SCHOOLS_DIRECTORY_WEBPAGE = 'http://rejskol.msmt.cz'
SCHOOLS_FORM_URI = '/VREJVerejne/VerejneRozhrani.aspx'
SCHOOLS_LIST_URI = '/VREJVerejne/VerejnySeznam.aspx'
SCHOOLS_TYPE_KINDERGARTEN = 'A'
SCHOOLS_RECORDS_PER_PAGE = 400
EXPORT_FILENAME = 'materskeskoly' # timestamp and .cvs or .json added during export
EXPORT_CSV_ORDER = ['city', 'name', 'address', 'capacity']
TOWN_BUDGET_FORM_URI = 'http://www.rozpocetobce.cz/zebricky'
