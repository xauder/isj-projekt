# -*- coding: utf-8 -*-
#!/usr/bin/env python3 
#-----

from bs4 import BeautifulSoup
from settings import *

import requests

#-----
# Base class for dowloading HTML code of pages from URLs
# All other classes which download some webpages can inherit from this class
#-----

class Page(object):

  session = requests.session()

  #-----
  # Init sets the URL address of the page to load and a loaded flag to 
  # False which means the page wasn't downloaded yet
  #-----

  def __init__(self, url):
    self.url = url
    self.loaded = False

  #-----
  # Param of this functions says if we want to load the page from the URL 
  # again. If set to False the already loaded version is returned and no 
  # request is made, bu of course the page must be downloaded at least once
  #-----

  def loadPage(self, method='GET', params=None, refresh=False, encoding=None):
    if not self.loaded or refresh:
      if method == 'GET':
        response = self.session.get(self.url, params=params)
      else:
        response = self.session.post(self.url, data=params)
      if response.status_code == 200:
        if encoding:
          response.encoding = encoding
        self.soup = BeautifulSoup(response.text)
        self.loaded = True
      else:
        self.loaded = False
    if self.loaded:
      return self.soup
    return False

  #-----
  # Returns an element of the page whose name attribute matches the value
  # given by parameter. Returns False if the page is not loaded yet
  #-----

  def getElementByName(self, name):
    if not self.loaded:
      return False
    attrs = dict(name=name)
    return self.soup.find(attrs=attrs)
