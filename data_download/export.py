# -*- coding: utf-8 -*-
#!/usr/bin/env python3
#-----

import csv
import json
import datetime
from settings import EXPORT_FILENAME, EXPORT_CSV_ORDER

#-----
# Data export to specific format
# This function takes an list of dict as argument and writes its content
# into a file. Filename is globaly set in settings.py and timestamp +
# subfix added. Optional settings for encoding and separator.
#-----
def exportCsv(listOfDicts, order=None, encodOut='utf-8', sepOut=','):
    if not order:
        order = EXPORT_CSV_ORDER
    with open(EXPORT_FILENAME + datetime.datetime.now().strftime('-%Y-%m-%d') + '.csv', 'w', encoding=encodOut) as fileOut:
        csvOut = csv.DictWriter(fileOut, order, delimiter=sepOut)
        csvOut.writeheader()
        for dictAsLine in listOfDicts:
             csvOut.writerow(dictAsLine)

#-----
# Same as above. Function for Json export, optional encoding
#-----
def exportJson(listOfDicts, encodOut='utf-8'):
    with open(EXPORT_FILENAME + datetime.datetime.now().strftime('-%Y-%m-%d') + '.json', 'w', encoding=encodOut) as jsonOut:
        json.dump(listOfDicts, jsonOut, ensure_ascii=False)
