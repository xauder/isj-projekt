# -*- coding: utf-8 -*-
#!/usr/bin/env python3
#-----

import os

thisFilePath = os.path.abspath(__file__)
thisFileDir = os.path.dirname(thisFilePath)

os.sys.path.insert(0, thisFileDir)

from bs4 import BeautifulSoup
from page import Page
from settings import TOWN_BUDGET_FORM_URI

import re
import requests

#-----
# Loads and parses information from the search form page. Does a GET
# request in purpose of getting CSV file which proceeds to a list of
# dictionaries
#-----

class BudgetPage(Page):
    #-----
    # Sets the basic attributes for form request. The empty ones are
    # fetched dynamicaly after loading form page
    #-----

    def __init__(self):
        Page.__init__(self, '')
        self.session = requests.session()
        self.getData = {
            'filtr_obci[budget_item]': '',
            'filtr_obci[budget_item_purpose]': '',
            'filtr_obci[per_inhabitant]': '0',
            'filtr_obci[year]': '',
            'filtr_obci[sort]': 'desc',
            'csv_export': '1'
        }
        self.url = TOWN_BUDGET_FORM_URI
        self.csvSite = False
        self.refresh = False

    #-----
    # The main function of the process. Loads the CSV file and creates
    # a list of dictionaries containing desired date
    #-----

    def parseCsv(self):
        self.refresh = True
        rawCsv = self.loadPage(encoding='windows-1250')
        self.refresh = False
        self.csvSite=True
        usableCsv = str(rawCsv).replace('"', '').replace(',', '.')
        csvLines = usableCsv.splitlines()[2:-1]
        cities = list()
        for csvLine in csvLines:
            cityDataList = csvLine.split(';')
            city = dict(
                name=cityDataList[0],
                population=int(cityDataList[4]),
                preschoolExpenses=float(cityDataList[3]),
                region=cityDataList[2])
            cities.append(city)
        return cities

    #-----
    # Sends a HTTP request for form page and returns a BeautifulSoup
    # HTML object
    #-----

    def loadPage(self, encoding=None):
        if not self.url:
            self.url = TOWN_BUDGET_FORM_URI
        params = self.getGetParams()
        return Page.loadPage(self, method='GET', params=params, refresh=self.refresh, encoding=encoding)

    #-----
    # Prepares GET paremeters for the search request, Order of items in
    # form can change so that's why it's good to fetch them dynamicaly
    #-----

    def getGetParams(self):
        if not self.loaded:
            return None
        if self.csvSite:
            return self.getData
        self.getData['filtr_obci[year]'] = self.getElementByName('filtr_obci[year]').find_all('option')[-1].string
        self.getData['filtr_obci[budget_item]'] = self.matchOption('filtr_obci[budget_item]', 'Výdaje')
        self.getData['filtr_obci[budget_item_purpose]'] = self.matchOption('filtr_obci[budget_item_purpose]', '.*Předškolní zařízení')
        return self.getData

    #-----
    # Helper for matching option value
    #-----

    def matchOption(self, idElement, idString):
        for item in self.getElementByName(idElement).find_all('option'):
            if re.match(idString, item.string):
                return item['value']
