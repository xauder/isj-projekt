# -*- coding: utf-8 -*-
#!/usr/bin/env python3
#-----

import sys
import subprocess
from app import app


if __name__ == '__main__':
    app.run(debug = True)
